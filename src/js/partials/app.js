$('.slider-top').owlCarousel({
    loop: true,
    margin: 0,
    responsiveClass: true,
    items: 1,
    nav: true,
    navText: ["", ""],
    autoplay: true,
    autoplayTimeout: 10000,
    autoplayHoverPause: true
});
$('.main-slider').owlCarousel({ 
    loop: true,
    responsiveClass: true,
    nav: true,
    navText: ["", ""],
    autoplay: false,
    responsive:{
        0:{
            items: 2,
            autoWidth:true,
            margin: 0
        },
        480:{
            autoWidth:true,
            items: 2,
            margin: 10
        },
        767:{               
            items: 2,
            margin: 20
        },
        1170:{
            margin: 30,
            items:3
        }
    }
});

$(".nav_mobile").click(function(){
    $(".menu-top").toggleClass("active"),
        $(".nav_mobile").toggleClass("active");
});